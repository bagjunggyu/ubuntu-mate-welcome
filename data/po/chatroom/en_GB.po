# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2016-03-19 15:09+0000\n"
"Last-Translator: Martin Wimpress <code@flexion.org>\n"
"Language-Team: English (United Kingdom) (http://www.transifex.com/ubuntu-mate/ubuntu-mate-welcome/language/en_GB/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: en_GB\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: chatroom.html:17
msgid "Chat Room"
msgstr "Chat Room"

#: chatroom.html:25
msgid "Chat with fellow users"
msgstr "Chat with fellow users"

#: chatroom.html:26
msgid "This IRC client is pre-configured so you can jump into our"
msgstr "This IRC client is pre-configured so you can jump into our"

#: chatroom.html:27
msgid "channel on"
msgstr "channel on"

#: chatroom.html:28
msgid ""
"Most of the Ubuntu MATE team are in here but they have real lives too. If "
"you have a question, do ask."
msgstr "Most of the Ubuntu MATE team are in here but they have real lives too. If you have a question, do ask."

#: chatroom.html:30
msgid ""
"However, it may take a while for someone to reply. Just be patient and don't"
" disconnect right away."
msgstr "However, it may take a while for someone to reply. Just be patient and don't disconnect right away."

#: chatroom.html:34
msgid ""
"It appears you are not connected to the Internet. Please check your "
"connection to access this content."
msgstr "It appears you are not connected to the Internet. Please check your connection to access this content."

#: chatroom.html:35
msgid "Sorry, Welcome was unable to establish a connection."
msgstr "Sorry, Welcome was unable to establish a connection."

#: chatroom.html:36
msgid "Retry"
msgstr "Retry"

#: chatroom.html:39
msgid "Join the IRC in Hexchat"
msgstr "Join the IRC in Hexchat"
