# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2016-03-19 15:09+0000\n"
"Last-Translator: Martin Wimpress <code@flexion.org>\n"
"Language-Team: Chinese (Taiwan) (http://www.transifex.com/ubuntu-mate/ubuntu-mate-welcome/language/zh_TW/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: zh_TW\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: community.html:17
msgid "Community"
msgstr "社群"

#: community.html:25
msgid "Forum"
msgstr "論壇"

#: community.html:26
msgid ""
"If you want to chat with the Ubuntu MATE team and other members of our "
"growing community, this is where to go. The best place to play an active "
"role in the Ubuntu MATE community and to help shape its direction is via our"
" forum."
msgstr "如果您想要和Ubuntu MATE團隊或是其他社群裡的成員們聊天，這裡是你的好選擇。您可以在我們的論壇扮演一個活躍的角色並且幫忙形塑Ubuntu MATE社群的方向"

#: community.html:31
msgid ""
"It appears you are not connected to the Internet. Please check your "
"connection to access this content."
msgstr "沒有連接到網際網路。請檢查您的連線"

#: community.html:32
msgid "Sorry, Welcome was unable to establish a connection."
msgstr "抱歉，\"歡迎\"無法建立連線"

#: community.html:33
msgid "Retry"
msgstr "重試"

#: community.html:38
msgid "Social Networks"
msgstr "社群網路"

#: community.html:39
msgid "Ubuntu MATE is active on the following social networks."
msgstr "Ubuntu MATE活躍於以下這些社群網路"
